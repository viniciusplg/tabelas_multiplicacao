function botao_acao() {
  let vlr_linhas = document.querySelector("input#linhas");
  let vlr_colunas = document.querySelector("input#colunas");
  if (vlr_colunas.value <= 0 || vlr_linhas.value <= 0) {
    alert("Digite colunas e linhas maiores que 0!");
  } else {
    let remover_tabela = document.querySelector("table#mult_tab");
    if (isNaN(remover_tabela)) {
      remover_tabela.remove();
    }
    let new_tab = document.createElement("table");
    new_tab.id = "mult_tab";
    document.body.appendChild(new_tab);

    let new_tr = [];
    let new_td = [];
    let td = 0;

    for (let tr = 0; tr <= Number(vlr_linhas.value) + 1; tr++) {
      new_tr[tr] = document.createElement("tr");
      new_tab.appendChild(new_tr[tr]);
      for (td = 0; td <= Number(vlr_colunas.value) + 1; td++) {
        if (tr == 0 && td == 0) {
          //Símbolo da primeira coluna e linha
          new_td[td * (tr + 1)] = document.createElement("td");
          new_td[td * (tr + 1)].innerHTML = "&times";
          new_td[td * (tr + 1)].id = "X";
          new_tr[tr].appendChild(new_td[td * (tr + 1)]);
        } else if (td == 0) {
          //Header Coluna
          new_td[td * (tr + 1)] = document.createElement("td");
          new_td[td * (tr + 1)].innerHTML = tr - 1;
          new_td[td * (tr + 1)].className = "header";
          new_tr[tr].appendChild(new_td[td * (tr + 1)]);
        } else if (tr == 0) {
          //Header Linha
          new_td[td * (tr + 1)] = document.createElement("td");
          new_td[td * (tr + 1)].innerHTML = td - 1;
          new_td[td * (tr + 1)].className = "header";
          new_tr[tr].appendChild(new_td[td * (tr + 1)]);
        } else {
          // Cálculos da multiplicação
          new_td[td * (tr + 1)] = document.createElement("td");
          new_td[td * (tr + 1)].innerHTML = (td - 1) * (tr - 1);
          new_td[td * (tr + 1)].className = "tabela_geral";
          new_tr[tr].appendChild(new_td[td * (tr + 1)]);
        }
      }
    }
  }
}

const header = document.getElementsByClassName("header");
const tabela_geral = document.getElementsByClassName("tabela_geral");

function botao_padrao() {
  X.style.background = "greenyellow";
  for (let i = 0; i < header.length; i++) {
    header[i].style.background = "lightblue";
  }
  for (let i = 0; i < tabela_geral.length; i++) {
    tabela_geral[i].style.background = "white";
  }
}

function botao_estilo1() {
  X.style.background = "yellow";
  for (let i = 0; i < header.length; i++) {
    header[i].style.background = "limegreen";
  }
  for (let i = 0; i < tabela_geral.length; i++) {
    tabela_geral[i].style.background = "darkturquoise";
  }
}

function botao_estilo2() {
  X.style.background = "lightseagreen";
  for (let i = 0; i < header.length; i++) {
    header[i].style.background = "salmon";
  }
  for (let i = 0; i < tabela_geral.length; i++) {
    tabela_geral[i].style.background = "khaki";
  }
}

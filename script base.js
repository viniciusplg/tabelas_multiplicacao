let new_tab = document.createElement('table')
new_tab.id = 'mult_tab'
document.body.appendChild(new_tab)

let new_tr = []
let new_td = []
let td = 0

for (let tr = 0; tr <= 11; tr++) {
    new_tr[tr] = document.createElement('tr')
    new_tab.appendChild(new_tr[tr])
    for (td = 0; td <= 11; td++) {
        if (tr == 0 && td == 0) { //Símbolo da primeira coluna e linha
            new_td[td * (tr + 1)] = document.createElement('td')
            console.log(new_td[td * (tr + 1)])
            new_td[td * (tr + 1)].innerHTML = 'X'
            new_td[td * (tr + 1)].id = 'X'
            new_tr[tr].appendChild(new_td[td * (tr + 1)])
        } else if (td == 0) { //Header Coluna
            new_td[td * (tr + 1)] = document.createElement('td')
            new_td[td * (tr + 1)].innerHTML = tr - 1
            new_td[td * (tr + 1)].id = 'header'
            new_tr[tr].appendChild(new_td[td * (tr + 1)])
        } else if (tr == 0) { //Header Linha
            new_td[td * (tr + 1)] = document.createElement('td')
            new_td[td * (tr + 1)].innerHTML = td - 1
            new_td[td * (tr + 1)].id = 'header'
            new_tr[tr].appendChild(new_td[td * (tr + 1)])
        } else { // Cálculos da multiplicação
            new_td[td * (tr + 1)] = document.createElement('td')
            new_td[td * (tr + 1)].innerHTML = (td-1) * (tr-1)
            new_td[td * (tr + 1)].id = 'tabela_geral'
            new_tr[tr].appendChild(new_td[td * (tr + 1)])
        }
    }
}